package com.noahjutz.findchip.ui.device_details

import android.bluetooth.BluetoothDevice
import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.VolumeOff
import androidx.compose.material.icons.filled.VolumeUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.map
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf
import kotlin.math.absoluteValue

@Composable
fun DeviceDetails(
    device: BluetoothDevice,
    viewModel: DeviceDetailsViewModel = getViewModel { parametersOf(device) },
    popBackStack: () -> Unit,
) {
    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = popBackStack) {
                        Icon(
                            Icons.Default.ArrowBack,
                            null
                        )
                    }
                },
                title = { Text(viewModel.deviceName) }
            )
        },
        floatingActionButton = {
            val isBeeping by viewModel.isBeeping.collectAsState()
            ExtendedFloatingActionButton(
                onClick = { if (isBeeping) viewModel.stopBeep() else viewModel.startBeep() },
                icon = {
                    Icon(
                        if (isBeeping) Icons.Default.VolumeOff else Icons.Default.VolumeUp,
                        null
                    )
                },
                text = { Text("${if (isBeeping) "Stop" else "Start"} beeping") },
            )
        }
    ) {
        val isConnected by viewModel.isConnected.collectAsState(false)
        when {
            !isConnected -> ConnectingAlert(popBackStack)
            else -> DeviceDetailsContent(viewModel)
        }
    }
}

@Composable
private fun DeviceDetailsContent(
    viewModel: DeviceDetailsViewModel
) {
    val signalStrength by viewModel.rssi.map { 100 - it.absoluteValue }.collectAsState(initial = 0)
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text("Signal strength: $signalStrength%")

            SignalStrengthIndicator(signalStrength)
        }
    }
}

@Composable
fun SignalStrengthIndicator(signalStrength: Int) {
    Box(contentAlignment = Alignment.Center) {
        SignalStrengthIndicatorBackground()
        SignalStrengthIndicatorForeground(signalStrength)
    }
}

@Composable
fun SignalStrengthIndicatorBackground() {
    val infiniteTransition = rememberInfiniteTransition()
    val alpha by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 0.5f,
        animationSpec = infiniteRepeatable(
            animation = tween(2000, easing = LinearOutSlowInEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    val radius by infiniteTransition.animateFloat(
        initialValue = 300f,
        targetValue = 350f,
        animationSpec = infiniteRepeatable(
            animation = tween(2000, easing = LinearOutSlowInEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    val color = MaterialTheme.colors.primary
    Canvas(Modifier.size(300.dp)) {
        drawCircle(
            color = color.copy(alpha = alpha),
            radius = radius
        )
    }
}

@Composable
fun SignalStrengthIndicatorForeground(signalStrength: Int) {
    val color = MaterialTheme.colors.primary
    val radius =
        animateFloatAsState((signalStrength * 3).toFloat(), animationSpec = TweenSpec(2000)).value
    Canvas(Modifier.size(300.dp)) {
        drawCircle(
            color = color,
            radius = radius
        )
        drawCircle(
            color = color.copy(alpha = 0.1f),
            radius = 300f
        )
    }
}

@Composable
private fun ConnectingAlert(
    popBackStack: () -> Unit
) {
    AlertDialog(
        title = { Text("Connecting...") },
        text = {
            LinearProgressIndicator(Modifier.fillMaxWidth())
        },
        dismissButton = {
            TextButton(onClick = popBackStack) {
                Text("Cancel")
            }
        },
        confirmButton = {},
        onDismissRequest = popBackStack
    )
}
