package com.noahjutz.findchip.ui.about

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.noahjutz.findchip.BuildConfig
import com.noahjutz.findchip.R

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AboutApp(
    popBackStack: () -> Unit,
    openUrl: (String) -> Unit,
) {
    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = popBackStack) { Icon(Icons.Default.ArrowBack, null) }
                },
                title = { Text("About") },
            )
        }
    ) {
        var showDependencyDialog by remember { mutableStateOf(false) }
        var showSupportedDevicesDialog by remember { mutableStateOf(false) }
        when {
            showDependencyDialog -> DependencyDialog(
                openUrl = openUrl,
                onDismiss = { showDependencyDialog = false }
            )
            showSupportedDevicesDialog -> SupportedDevicesDialog(
                onDismiss = { showSupportedDevicesDialog = false }
            )
        }
        Column(Modifier.verticalScroll(rememberScrollState())) {
            Box(
                Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                contentAlignment = Alignment.Center
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Image(
                        imageVector = ImageVector.vectorResource(R.drawable.ic_launcher_foreground_sysicon),
                        contentDescription = "Logo",
                        modifier = Modifier
                            .size(60.dp)
                            .clip(CircleShape)
                            .background(colorResource(R.color.ic_launcher_background)),
                        contentScale = ContentScale.FillBounds
                    )
                    Spacer(Modifier.width(12.dp))
                    ProvideTextStyle(typography.h3) {
                        Text("Findchip")
                    }
                }
            }

            Divider()

            Card(
                Modifier.padding(16.dp)
            ) {
                Text(
                    "Findchip is an open-source replacement for proprietary location tag apps. It displays signal strength based on the RSSI and allows for sending signals to start/stop beeping. The BLE communication was reverse engineered using Wireshark.",
                    modifier = Modifier.padding(16.dp)
                )
            }

            ListItem(
                Modifier.clickable {},
                text = { Text("Author") },
                secondaryText = { Text("Noah Jutz") },
                icon = { Icon(Icons.Default.Face, null) },
            )
            ListItem(
                Modifier.clickable {},
                text = { Text("Version") },
                secondaryText = { Text(BuildConfig.VERSION_NAME) },
                icon = { Icon(Icons.Default.Update, null) },
            )
            ListItem(
                Modifier.clickable { openUrl("https://codeberg.org/noahjutz/Findchip/raw/branch/main/LICENSE") },
                text = { Text("License") },
                secondaryText = { Text("GPL-3.0") },
                icon = { Icon(Icons.Default.LockOpen, null) },
                trailing = {
                    Icon(
                        Icons.Default.Launch,
                        null,
                        tint = LocalContentColor.current.copy(alpha = 0.5f)
                    )
                },
            )
            ListItem(
                Modifier.clickable { showDependencyDialog = true },
                text = { Text("Dependencies") },
                secondaryText = { Text("Open source licenses") },
                icon = { Icon(Icons.Default.List, null) },
            )
            ListItem(
                Modifier.clickable { showSupportedDevicesDialog = true },
                text = { Text("Supported devices") },
                secondaryText = { Text("1 supported device") },
                icon = { Icon(Icons.Default.Check, null) },
            )
            ListItem(
                Modifier.clickable { openUrl("https://codeberg.org/noahjutz/Findchip") },
                text = { Text("Source Code") },
                secondaryText = { Text("GitHub") },
                icon = { Icon(Icons.Default.Code, null) },
                trailing = {
                    Icon(
                        Icons.Default.Launch,
                        null,
                        tint = LocalContentColor.current.copy(alpha = 0.5f)
                    )
                },
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DependencyDialog(
    openUrl: (String) -> Unit,
    onDismiss: () -> Unit
) {
    AlertDialog(
        onDismissRequest = onDismiss,
        title = { Text("Dependencies") },
        text = {
            Column(Modifier.verticalScroll(rememberScrollState())) {
                ListItem(
                    Modifier.clickable { openUrl("https://developer.android.com/jetpack/androidx/") },
                    text = { Text("AndroidX") },
                    secondaryText = { Text("Apache 2.0") },
                    trailing = {
                        Icon(
                            Icons.Default.Launch,
                            null,
                            tint = LocalContentColor.current.copy(alpha = 0.5f)
                        )
                    },
                )
                ListItem(
                    Modifier.clickable { openUrl("https://insert-koin.io/") },
                    text = { Text("koin") },
                    secondaryText = { Text("Apache 2.0") },
                    trailing = {
                        Icon(
                            Icons.Default.Launch,
                            null,
                            tint = LocalContentColor.current.copy(alpha = 0.5f)
                        )
                    },
                )
            }
        },
        confirmButton = {},
        dismissButton = {
            TextButton(onClick = onDismiss) {
                Text("Dismiss")
            }
        }
    )
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SupportedDevicesDialog(onDismiss: () -> Unit) {
    AlertDialog(
        onDismissRequest = onDismiss,
        title = { Text("Supported Devices") },
        text = {
            Column(Modifier.verticalScroll(rememberScrollState())) {
                ListItem(
                    Modifier.clickable { },
                    text = { Text("iFindU") },
                )
                Text("If your device is not on the list, please consider contributing.")
            }
        },
        confirmButton = {},
        dismissButton = {
            TextButton(onClick = onDismiss) {
                Text("Dismiss")
            }
        }
    )
}
